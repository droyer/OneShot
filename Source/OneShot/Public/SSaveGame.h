// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class ONESHOT_API USSaveGame : public USaveGame
{
	GENERATED_BODY()

private:
	static uint32 UserIndex;

	FString SlotName;

public:
	UPROPERTY()
	FText IpAddress;

	UPROPERTY()
	float Volume;

	UPROPERTY()
	float MouseSpeed;

	UPROPERTY()
	FString KeyboardMode;

	UPROPERTY()
	FString PlayerName;

	UPROPERTY()
	FColor PlayerColor;

	static USSaveGame* GetInstance(FString NewSlotName);

	void Save();

	static bool Exist(FString SlotName);
};

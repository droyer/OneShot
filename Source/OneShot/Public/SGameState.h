// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SGameState.generated.h"

class APlayerController;
class ASHUD;

/**
 * 
 */
UCLASS()
class ONESHOT_API ASGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	ASGameState();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnPlayerConnect(APlayerController* NewPlayer, const FString& PlayerName, FColor PlayerColor);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnPlayerDisconnect(const FString& PlayerName, FColor PlayerColor);

protected:
	virtual void BeginPlay() override;

private:
	APlayerController* PlayerController;

	ASHUD* SHUD;
};

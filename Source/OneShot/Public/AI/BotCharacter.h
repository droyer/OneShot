// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/BaseCharacter.h"
#include "BotCharacter.generated.h"

class UPawnSensingComponent;
class UBehaviorTree;

/**
 * 
 */
UCLASS()
class ONESHOT_API ABotCharacter : public ABaseCharacter
{
	GENERATED_BODY()
	
private:
	ABotCharacter();

	FTimerHandle FireTimer;

	UFUNCTION()
	void FireHandle();

	UPROPERTY(Replicated)
	float AimPitch;

	UPROPERTY(VisibleAnywhere, Category = "AI")
	UPawnSensingComponent* PawnSensingComp;
	
	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* BehaviorTree;

protected:
	virtual void BeginPlay() override;

public:
	virtual FRotator GetViewRotation() const override;

	virtual FRotator GetBaseAimRotation() const override;

	FORCEINLINE UBehaviorTree* GetBehaviorTree() { return BehaviorTree; }
	
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuMainWidget.generated.h"

class UButton;
class UEditableTextBox;
class USSaveGame;

/**
 * 
 */
UCLASS()
class ONESHOT_API UMenuMainWidget : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY()
	USSaveGame* SaveGame_IP;

	UButton* Button_Solo;

	UFUNCTION()
	void OnButtonClicked_Solo();

	UButton* Button_ListenServer;

	UFUNCTION()
	void OnButtonClicked_ListenServer();

	UButton* Button_JoinServer;

	UFUNCTION()
	void OnButtonClicked_JoinServer();

	UEditableTextBox* TextBox_IP;

	UButton* Button_Options;

	UFUNCTION()
	void OnButtonClicked_Options();

	UButton* Button_PlayerSettings;

	UFUNCTION()
	void OnButtonClicked_PlayerSettings();

	UButton* Button_Exit;

	UFUNCTION()
	void OnButtonClicked_Exit();

	void SaveSettings();

protected:
	virtual void NativeConstruct() override;

	virtual void NativeDestruct() override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScoreboardItemWidget.generated.h"

class UImage;
class UTextBlock;

/**
 * 
 */
UCLASS()
class ONESHOT_API UScoreboardItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
private:
	UImage* Image_PlayerColor;

	UTextBlock* TextBlock_Pseudo;

	UTextBlock* TextBlock_Score;

	UTextBlock* TextBlock_Kills;

	UTextBlock* TextBlock_Deaths;

	UTextBlock* TextBlock_Ping;

protected:
	virtual void NativeConstruct() override;

public:
	void SetPlayerColor(FColor Color);

	void SetPseudo(FString Name);
	
	void SetScore(float Value);

	void SetKills(uint8 Value);

	void SetDeaths(uint8 Value);

	void SetPing(uint8 Value);
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuPlayerSettingsWidget.generated.h"

class USSaveGame;
class UEditableTextBox;
class UImage;
class UButton;

/**
 * 
 */
UCLASS()
class ONESHOT_API UMenuPlayerSettingsWidget : public UUserWidget
{
	GENERATED_BODY()

private:
	UPROPERTY()
	USSaveGame* SaveGame_PlayerSettings;

	UEditableTextBox* TextBox_Pseudo;

	UEditableTextBox* TextBox_Red;

	UEditableTextBox* TextBox_Green;

	UEditableTextBox* TextBox_Blue;

	UImage* Image_Color;

	UButton* Button_Apply;

	UButton* Button_Back;

	UFUNCTION()
	void OnTextBoxTextChanged_Red(const FText& Text);

	UFUNCTION()
	void OnTextBoxTextChanged_Green(const FText& Text);

	UFUNCTION()
	void OnTextBoxTextChanged_Blue(const FText& Text);

	UFUNCTION()
	void OnButtonClicked_Apply();

	UFUNCTION()
	void OnButtonClicked_Back();
	
protected:
	virtual void NativeConstruct() override;

	virtual void NativeDestruct() override;
};

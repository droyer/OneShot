// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"


class ASWeapon;
class USoundBase;
class ASPlayerState;
class UPawnNoiseEmitterComponent;


USTRUCT()
struct FShotResult
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FVector_NetQuantizeNormal ShotDirection;

	UPROPERTY()
	FVector_NetQuantize ImpactPoint;

	UPROPERTY()
	FName BoneName;

	UPROPERTY()
	ASPlayerState* KillerInfo;
};

UCLASS()
class ONESHOT_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()


private:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<ASWeapon> StartWeaponClass;

	bool bIsDead;

	UFUNCTION(NetMulticast, Reliable)
	void MulticastDie(FShotResult ShotInfo);

	UMaterialInstanceDynamic* Material;

	ASPlayerState* SPlayerState;

	UPawnNoiseEmitterComponent* NoiseEmitterComp;

public:
	ABaseCharacter();

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(Replicated)
	ASWeapon* CurrentWeapon;

public:

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	
	void BeginCrouch();

	void EndCrouch();

	void StartFire();

	void StopFire();

	FORCEINLINE bool IsDead() const { return bIsDead; }
};

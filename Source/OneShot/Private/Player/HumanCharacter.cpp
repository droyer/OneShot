// Fill out your copyright notice in the Description page of Project Settings.

#include "HumanCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/CapsuleComponent.h"
#include "SSaveGame.h"
#include "SPlayerController.h"
#include "Kismet/GameplayStatics.h"

//TMP
#include "BotController.h"

AHumanCharacter::AHumanCharacter()
{
	CameraBoomComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoomComp->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
	CameraBoomComp->bUsePawnControlRotation = true;
	CameraBoomComp->TargetArmLength = 0;
	CameraBoomComp->bEnableCameraLag = true;
	CameraBoomComp->CameraLagSpeed = 30.0f;
	CameraBoomComp->SetupAttachment(GetCapsuleComponent());

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->SetupAttachment(CameraBoomComp);

	GetMesh()->SetOwnerNoSee(true);
	GetMesh()->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::AlwaysTickPoseAndRefreshBones;
}

void AHumanCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AHumanCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AHumanCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &AHumanCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &AHumanCharacter::LookUp);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ABaseCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ABaseCharacter::EndCrouch);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABaseCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ABaseCharacter::StopFire);
}

void AHumanCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (IsLocallyControlled())
	{
		ASPlayerController* PlayerController = Cast<ASPlayerController>(GetController());
		if (PlayerController)
		{
			PlayerController->UpdateFocus();
		}

		UpdateSettings();

		UGameplayStatics::SpawnSound2D(GetWorld(), SpawnSound);
	}

	//TMP
	if (Role == ROLE_Authority && IsLocallyControlled())
	{
		for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
		{
			ABotController* BotController = Cast<ABotController>(*It);
			if (BotController)
			{
				BotController->SetBlackboardPlayer(this);
			}
		}
	}
}

void AHumanCharacter::OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnStartCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	CameraBoomComp->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
}

void AHumanCharacter::OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnEndCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	CameraBoomComp->SetRelativeLocation(FVector(0.0f, 0.0f, BaseEyeHeight));
}

void AHumanCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AHumanCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AHumanCharacter::Turn(float Value)
{
	if (Value != 0.0f)
	{
		AddControllerYawInput(Value * MouseSpeed);
	}
}

void AHumanCharacter::LookUp(float Value)
{
	if (Value != 0.0f)
	{
		AddControllerPitchInput(Value * MouseSpeed);
	}
}

void AHumanCharacter::UpdateSettings()
{
	USSaveGame* SaveGame_Options = USSaveGame::GetInstance("Options");
	MouseSpeed = SaveGame_Options->MouseSpeed;

	Controller->InputComponent->AxisBindings.Empty();

	if (SaveGame_Options->KeyboardMode == TEXT("QWERTY"))
	{
		Controller->InputComponent->BindAxis("MoveForwardWS", this, &AHumanCharacter::MoveForward);
		Controller->InputComponent->BindAxis("MoveRightDA", this, &AHumanCharacter::MoveRight);
	}
	else
	{
		Controller->InputComponent->BindAxis("MoveForwardZS", this, &AHumanCharacter::MoveForward);
		Controller->InputComponent->BindAxis("MoveRightDQ", this, &AHumanCharacter::MoveRight);
	}
}
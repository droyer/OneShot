// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacter.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "SWeapon.h"
#include "OneShot.h"
#include "SPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "SSaveGame.h"
#include "SPlayerState.h"
#include "SHUD.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "BotController.h"
#include "Components/PawnNoiseEmitterComponent.h"

ABaseCharacter::ABaseCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	BaseEyeHeight = 80.0f;
	CrouchedEyeHeight = 50.0f;

	GetCharacterMovement()->JumpZVelocity = 550.0f;
	GetCharacterMovement()->AirControl = 0.5f;
	GetCharacterMovement()->GravityScale = 2.0f;
	GetCharacterMovement()->MaxAcceleration = 4096.0f;
	GetCharacterMovement()->BrakingFrictionFactor = 0.0f;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCharacterMovement()->CrouchedHalfHeight = 60.0f;
	GetCharacterMovement()->bCanWalkOffLedgesWhenCrouching = true;

	SetReplicates(true);
	SetReplicateMovement(true);

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	NoiseEmitterComp = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("NoiseEmitterComp"));
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;
		CurrentWeapon = GetWorld()->SpawnActor<ASWeapon>(StartWeaponClass, SpawnParams);
	}

	Material = UMaterialInstanceDynamic::Create(GetMesh()->GetMaterial(0), this);
	GetMesh()->SetMaterial(0, Material);
}

void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SPlayerState == nullptr)
	{
		SPlayerState = Cast<ASPlayerState>(PlayerState);
	}

	if (SPlayerState)
	{
		Material->SetVectorParameterValue("BodyColor", SPlayerState->PlayerColor);
	}
}

void ABaseCharacter::BeginCrouch()
{
	if (GetCharacterMovement()->IsMovingOnGround())
	{
		Crouch();
	}
}

void ABaseCharacter::EndCrouch()
{
	UnCrouch();
}

void ABaseCharacter::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void ABaseCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

float ABaseCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (bIsDead)
	{
		return 0.0f;
	}

	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	CurrentWeapon->SetLifeSpan(0.001f);

	SetReplicateMovement(false);
	
	if (!SPlayerState->bIsABot)
	{
		ASPlayerController* TargetPlayerController = Cast<ASPlayerController>(GetController());
		TargetPlayerController->Death();
	}
	else
	{
		ABotController* BotController = Cast<ABotController>(GetController());
		BotController->OnDeath();
	}
	SPlayerState->Deaths++;

	ASPlayerState* KillerPlayerState = nullptr;
	if (EventInstigator)
	{
		KillerPlayerState = Cast<ASPlayerState>(EventInstigator->PlayerState);
	}
	
	FPointDamageEvent PointDamage = *((FPointDamageEvent*)&DamageEvent);
	FShotResult ShotInfo;
	ShotInfo.ShotDirection = PointDamage.ShotDirection;
	ShotInfo.ImpactPoint = PointDamage.HitInfo.ImpactPoint;
	ShotInfo.BoneName = PointDamage.HitInfo.BoneName;
	ShotInfo.KillerInfo = KillerPlayerState;
	MulticastDie(ShotInfo);

	EPhysicalSurface SurfaceType = UPhysicalMaterial::DetermineSurfaceType(PointDamage.HitInfo.PhysMaterial.Get());
	bool bFireInHead = SurfaceType == SURFACE_FLESHHEAD;

	if (EventInstigator)
	{
		if (!EventInstigator->PlayerState->bIsABot)
		{
			ASPlayerController* KillerPlayerController = Cast<ASPlayerController>(EventInstigator);
			if (KillerPlayerController)
			{
				KillerPlayerController->ClientOnKill(bFireInHead);
			}
		}

		if (KillerPlayerState)
		{
			float DeltaScore = bFireInHead ? 2 : 1;
			KillerPlayerState->Score += DeltaScore;
			KillerPlayerState->Kills++;
		}
	}

	SetLifeSpan(10);

	return 0.0f;
}

void ABaseCharacter::MulticastDie_Implementation(FShotResult ShotInfo)
{
	bIsDead = true;

	if (ShotInfo.KillerInfo)
	{
		APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
		if (PlayerController)
		{
			ASHUD* HUD = Cast<ASHUD>(PlayerController->GetHUD());
			if (HUD)
			{
				HUD->NewShotInfo(ShotInfo.KillerInfo, SPlayerState);
			}
		}
	}

	if (IsLocallyControlled())
	{
		DisableInput(Cast<APlayerController>(GetController()));
	}

	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

	GetMesh()->SetCollisionProfileName("Ragdoll");

	SetActorEnableCollision(true);

	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->WakeAllRigidBodies();
	GetMesh()->bBlendPhysics = true;

	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (CharacterComp)
	{
		CharacterComp->StopMovementImmediately();
		CharacterComp->DisableMovement();
		CharacterComp->SetComponentTickEnabled(false);
	}

	GetMesh()->AddImpulseAtLocation(ShotInfo.ShotDirection * 50000, ShotInfo.ImpactPoint, ShotInfo.BoneName);
}

void ABaseCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseCharacter, CurrentWeapon);
}
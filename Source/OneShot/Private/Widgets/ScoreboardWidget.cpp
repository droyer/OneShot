// Fill out your copyright notice in the Description page of Project Settings.

#include "ScoreboardWidget.h"
#include "ScoreboardItemWidget.h"
#include "Components/VerticalBox.h"
#include "Blueprint/WidgetTree.h"
#include "SPlayerState.h"
#include "EngineUtils.h"

void UScoreboardWidget::NativeConstruct()
{
	Super::NativeConstruct();

	VerticalBox_Item = Cast<UVerticalBox>(WidgetTree->FindWidget("VerticalBox_Item"));

	GetWorld()->GetTimerManager().SetTimer(DrawTimer, this, &UScoreboardWidget::DrawItem, 0.25f, true, 0.0f);
}

void UScoreboardWidget::NativeDestruct()
{
	Super::NativeDestruct();

	GetWorld()->GetTimerManager().ClearTimer(DrawTimer);
}

void UScoreboardWidget::DrawItem()
{
	VerticalBox_Item->ClearChildren();

	TArray<ASPlayerState*> PlayerArray;

	for (TActorIterator<ASPlayerState> It(GetWorld()); It; ++It)
	{
		ASPlayerState* PlayerState = Cast<ASPlayerState>(*It);
		PlayerArray.Add(PlayerState);
	}

	PlayerArray.Sort([](const ASPlayerState& One, const ASPlayerState& Two) {
		return One.Score > Two.Score;
	});

	for (int32 i = 0; i < PlayerArray.Num(); ++i)
	{
		ASPlayerState* PlayerState = PlayerArray[i];
		if (PlayerState)
		{
			UScoreboardItemWidget* Item = CreateWidget<UScoreboardItemWidget>(GetOwningPlayer(), ScoreboardItemWidgetClass);
			VerticalBox_Item->AddChild(Item);
			Item->SetPlayerColor(PlayerState->PlayerColor);
			Item->SetPseudo(PlayerState->GetPlayerName());
			Item->SetScore(PlayerState->Score);
			Item->SetKills(PlayerState->Kills);
			Item->SetDeaths(PlayerState->Deaths);
			Item->SetPing(PlayerState->Ping);
		}
	}
}
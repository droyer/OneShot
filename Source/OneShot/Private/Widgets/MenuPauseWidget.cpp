// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuPauseWidget.h"
#include "Components/Button.h"
#include "Blueprint/WidgetTree.h"
#include "SPlayerController.h"
#include "SHUD.h"
#include "Kismet/GameplayStatics.h"

void UMenuPauseWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Button_Resume = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Resume")));
	Button_Options = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Options")));
	Button_MenuMain = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_MenuMain")));
	Button_Exit = Cast<UButton>(WidgetTree->FindWidget(TEXT("Button_Exit")));

	Button_Resume->OnClicked.AddDynamic(this, &UMenuPauseWidget::OnButtonClicked_Resume);
	Button_Options->OnClicked.AddDynamic(this, &UMenuPauseWidget::OnButtonClicked_Options);
	Button_MenuMain->OnClicked.AddDynamic(this, &UMenuPauseWidget::OnButtonClicked_MenuMain);
	Button_Exit->OnClicked.AddDynamic(this, &UMenuPauseWidget::OnButtonClicked_Exit);
}

void UMenuPauseWidget::NativeDestruct()
{
	Super::NativeDestruct();

	Button_Resume->OnClicked.Clear();
	Button_Options->OnClicked.Clear();
	Button_MenuMain->OnClicked.Clear();
	Button_Exit->OnClicked.Clear();
}

void UMenuPauseWidget::OnButtonClicked_Resume()
{
	ASPlayerController* PlayerController = Cast<ASPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		PlayerController->TogglePause();
	}
}

void UMenuPauseWidget::OnButtonClicked_Options()
{
	ASPlayerController* PlayerController = Cast<ASPlayerController>(GetOwningPlayer());
	if (PlayerController)
	{
		ASHUD* HUD = Cast<ASHUD>(PlayerController->GetHUD());
		if (HUD)
		{
			HUD->SpawnMenuOptions();
		}
	}
}

void UMenuPauseWidget::OnButtonClicked_MenuMain()
{
	UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
}

void UMenuPauseWidget::OnButtonClicked_Exit()
{
	GetOwningPlayer()->ConsoleCommand("quit");
}

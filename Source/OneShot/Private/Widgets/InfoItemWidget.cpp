// Fill out your copyright notice in the Description page of Project Settings.

#include "InfoItemWidget.h"
#include "Components/TextBlock.h"
#include "Blueprint/WidgetTree.h"
#include "SPlayerState.h"

void UInfoItemWidget::NativeConstruct()
{
	Super::NativeConstruct();

	TextBlock_One = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_One"));
	TextBlock_Two = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Two"));
	TextBlock_Three = Cast<UTextBlock>(WidgetTree->FindWidget("TextBlock_Three"));

	if (!GetWorld()->GetTimerManager().TimerExists(DestroyTimer))
	{
		GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &UInfoItemWidget::Destroy, 5.0f);
	}
}

void UInfoItemWidget::SetTextOneInfo(const FString& Text, FColor Color)
{
	if (!TextBlock_One)
	{
		return;
	}

	TextBlock_One->SetText(FText::FromString(Text));
	FSlateColor SlateColor(Color);
	TextBlock_One->SetColorAndOpacity(SlateColor);
	TextBlock_One->SetVisibility(ESlateVisibility::Visible);
}

void UInfoItemWidget::SetTextTwoInfo(const FString& Text, FColor Color)
{
	if (!TextBlock_Two)
	{
		return;
	}

	TextBlock_Two->SetText(FText::FromString(Text));
	FSlateColor SlateColor(Color);
	TextBlock_Two->SetColorAndOpacity(SlateColor);
	TextBlock_Two->SetVisibility(ESlateVisibility::Visible);
}

void UInfoItemWidget::SetTextThreeInfo(const FString& Text, FColor Color)
{
	if (!TextBlock_Three)
	{
		return;
	}

	TextBlock_Three->SetText(FText::FromString(Text));
	FSlateColor SlateColor(Color);
	TextBlock_Three->SetColorAndOpacity(SlateColor);
	TextBlock_Three->SetVisibility(ESlateVisibility::Visible);
}

void UInfoItemWidget::Destroy()
{
	RemoveFromParent();
}
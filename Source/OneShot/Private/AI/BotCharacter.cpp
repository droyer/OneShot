// Fill out your copyright notice in the Description page of Project Settings.

#include "BotCharacter.h"
#include "BotController.h"
#include "SWeapon.h"
#include "Net/UnrealNetwork.h"
#include "Perception/PawnSensingComponent.h"

ABotCharacter::ABotCharacter()
{
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	AIControllerClass = ABotController::StaticClass();

	SetReplicates(true);
	SetReplicateMovement(true);
}

void ABotCharacter::BeginPlay()
{
	Super::BeginPlay();

	//TMP
	if (Role == ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(FireTimer, this, &ABotCharacter::FireHandle, 3.0f, true);

		AimPitch = 0.0f;
	}
}

//TMP
void ABotCharacter::FireHandle()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

FRotator ABotCharacter::GetViewRotation() const
{
	FRotator Rot = Super::GetViewRotation();
	//Rot.Pitch = AimPitch;
	return Rot;
}

FRotator ABotCharacter::GetBaseAimRotation() const
{
	FRotator Rot = Super::GetBaseAimRotation();
	//Rot.Pitch = AimPitch;
	return Rot;
}

void ABotCharacter::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABotCharacter, AimPitch);
}
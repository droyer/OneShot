// Fill out your copyright notice in the Description page of Project Settings.

#include "SSaveGame.h"
#include "Kismet/GameplayStatics.h"

uint32 USSaveGame::UserIndex = 0;

USSaveGame* USSaveGame::GetInstance(FString NewSlotName)
{
	USSaveGame* Instance = nullptr;
	if (UGameplayStatics::DoesSaveGameExist(NewSlotName, UserIndex))
	{
		Instance = Cast<USSaveGame>(UGameplayStatics::LoadGameFromSlot(NewSlotName, UserIndex));
	}
	else
	{
		Instance = Cast<USSaveGame>(UGameplayStatics::CreateSaveGameObject(USSaveGame::StaticClass()));
		Instance->Volume = 1.0f;
		Instance->MouseSpeed = 1.0f;
		Instance->KeyboardMode = TEXT("QWERTY");
		Instance->PlayerColor = FColor::White;
	}
	Instance->SlotName = NewSlotName;
	return Instance;
}

void USSaveGame::Save()
{
	UGameplayStatics::SaveGameToSlot(this, SlotName, UserIndex);
}

bool USSaveGame::Exist(FString SlotName)
{
	return UGameplayStatics::DoesSaveGameExist(SlotName, UserIndex);
}

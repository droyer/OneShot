// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameState.h"
#include "SPlayerController.h"
#include "EngineUtils.h"
#include "SHUD.h"

ASGameState::ASGameState()
{

}

void ASGameState::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		SHUD = Cast<ASHUD>(PlayerController->GetHUD());
	}
}

void ASGameState::MulticastOnPlayerConnect_Implementation(APlayerController* NewPlayer, const FString& PlayerName, FColor PlayerColor)
{
	if (NewPlayer && NewPlayer->IsLocalController())
	{
		return;
	}

	if (PlayerController)
	{
		if (SHUD)
		{
			SHUD->NewTextInfo(PlayerName, PlayerColor, " is connected");
		}
	}
}

void ASGameState::MulticastOnPlayerDisconnect_Implementation(const FString& PlayerName, FColor PlayerColor)
{
	if (PlayerController)
	{
		if (SHUD)
		{
			SHUD->NewTextInfo(PlayerName, PlayerColor, " has disconnect");
		}
	}
}